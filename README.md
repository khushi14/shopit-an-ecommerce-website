 # ShopIT - An E-Commerce Website

A E-commerce Website using MERN stack.

### Tech Stack 
 
<img src="https://img.shields.io/badge/React-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" alt="react" />

<img src="https://img.shields.io/badge/Node.js-339933?style=for-the-badge&logo=nodedotjs&logoColor=white" alt="nodejs" />

<img src="https://img.shields.io/badge/Express-20232A?style=for-the-badge&logo=react&logoColor=61DAFB" alt="expressjs" />

 <img src="https://img.shields.io/badge/MongoDB-4EA94B?style=for-the-badge&logo=mongodb&logoColor=white" alt="mongodb" />

### Tools

Redux - for state management

Stripe - to handle our payments

Cloudinary - to handle our images

Postman - for API Testing

Mailtrap - for Mail Testing


### Features

- Build backend API using node and express
- Authentication & Authorization using Jwt
- Complete Cart & Checkout process
- Complete ratings & reviews system
- Add filters, search and pagination
- Forgot and Reset Password
- Add third party site like cloudinary to upload images
- Payment Integration using Stripe
- Complete Admin Dashboard to manage products, orders, reviews, users


