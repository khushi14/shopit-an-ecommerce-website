const Product = require('../models/product');
const cloudinary = require('cloudinary');

//  utils
const ErrorHandler = require('../utils/errorHandler')
const APIFeatures = require('../utils/apifeatures');

// create new product ---> /api/v1/admin/product
exports.createProduct = async(req,res, next)=>{

    try {
        const {name, price, description, category, stock, seller} = req.body;
        if(!name || !price || !description || !category || !stock || !seller  ){
            return next(new ErrorHandler('Please Fill All The Fields!', 404));
        }
        let images = [];
        if (typeof req.body.images === 'string'){
            images.push(req.body.images);
        }else{
            images = req.body.images;
        }

        let imagesLinks = [];
        for(let i=0; i<images.length; i++){
            const result = await cloudinary.v2.uploader.upload(images[i], {
                folder: 'products',
                crop: "scale"
            });

            imagesLinks.push({
                public_id: result.public_id,
                url: result.secure_url
            })
        }

        req.body.images = imagesLinks
        req.body.user = req.user.id;
        const product = await Product.create(req.body);
        return  res.status(200).json({
            success:  true,
            message: 'Product created successfully',
            product
    })

    } catch (error) {
      
        if (error.name === 'ValidationError') {
            const message = Object.values(error.errors).map(value => value.message);
            return res.status(500).json({
                message 
            })
        }
    }

   
}

// get all products  ---> /api/v1/products
exports.getProducts = async(req,res, next)=>{

    const resPerPage = 8;
    const productCount = await Product.countDocuments();

    try {
        const apiFeatures = new APIFeatures(Product.find(), req.query).search().filter()
        let products = await apiFeatures.query;
        // const products = await Product.find();

        const filteredProductsCount = products.length;
        apiFeatures.pagination(resPerPage)
        products = await apiFeatures.query.clone();   // to exceute the query again
        if(products.length > 0){
            res.status(200).json({
                success:  true,
                productCount,
                products,
                resPerPage,
                filteredProductsCount
            })
        }else{

            return next(new ErrorHandler('No Product is Available', 404));
        }
      
    } catch (error) {
        if (error.name === 'CastError') {
            const message = `Resource not found. Invalid: ${error.path}`
            return res.status(500).json({
                message 
            })
        }
        else{
            return res.status(500).json({
                message : error.message
            })
        }
    }
    
}

// get all products from admin side   ---> /api/v1/admin/products
exports.getAdminProducts = async(req,res, next)=>{

    try {
        const products = await Product.find();
        res.status(200).json({
            success:true,
            products
        })
    } catch (error) {
        if (error.name === 'CastError') {
            const message = `Resource not found. Invalid: ${error.path}`
            return res.status(500).json({
                message 
            })
        }
        else{
            return res.status(500).json({
                message : error.message
            })
        }
    }
    
}

// get a specifc product using product id  ---> /api/v1/product/:id
exports.getSpecificProduct = async(req,res, next)=>{

    try {
        const product = await Product.findById(req.params.id).populate('reviews.user','avatar');

        if(!product){
            return next(new ErrorHandler('Product Not Found', 404));
        }

        const recommendedProducts = await Product.find({category: product.category});
        

        res.status(200).json({
            success:  true,
            product,
            recommendedProducts :  recommendedProducts.filter((prod) => prod._id.toString() !== product._id.toString())
        })
    } catch (error) {
        if (error.name === 'CastError') {
            const message = `Resource not found. Invalid: ${error.path}`
            return res.status(500).json({
                message 
            })
        } else{
            return res.status(500).json({
                message : error.message
            })
        }
    }
    
}

// get products by category
exports.getSpecificProductByCategory = async(req,res, next)=>{

    try {
      
        const products = await Product.find({category : req.params.category});

        if(!products){
            return next(new ErrorHandler('Products Not Found', 404));
        }
        const productCount = products.length
        res.status(200).json({
            success:  true,
            productCount,
            products
        })
    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
     
    }
    
}

// update a product using product id  ----> /api/v1/admin/product/:id
exports.updateProduct = async(req,res, next)=>{

    try {

        const {name, price, description, category, stock, seller} = req.body;
        if(!name || !price || !description || !category || !stock || !seller  ){
            return next(new ErrorHandler('Please Fill All The Fields!', 404));
        }
        
        let product = await Product.findById(req.params.id);

        if (!product) {
            return next(new ErrorHandler('Product not found', 404));
        }

        let images = [];
        if (typeof req.body.images === 'string'){
            images.push(req.body.images);
        }else{
            images = req.body.images;
        }

        if(images !== undefined){
            // deleting images associated with the product
            for(let i=0;i<product.images.length;i++){
                await cloudinary.v2.uploader.destroy(product.images[i].public_id)
            }

            let imagesLinks = [];
            for(let i=0; i<images.length; i++){
                const result = await cloudinary.v2.uploader.upload(images[i], {
                    folder: 'products',
                    crop: "scale"
                });

                imagesLinks.push({
                    public_id: result.public_id,
                    url: result.secure_url
                })
            }

            req.body.images = imagesLinks
        }

        let updatedProduct = await Product.findByIdAndUpdate(req.params.id, req.body, 
            {
                new: true
            }
        );
    
        res.status(200).json({
            success:  true,
            message: "Product updated successfully",
            product : updatedProduct
        })
    } catch (error) {
        if (error.name === 'CastError') {
            const message = `Resource not found. Invalid: ${error.path}`
            return res.status(500).json({
                message 
            })
        } else{
            return res.status(500).json({
                message : error.message
            })
        }
    }
    
}

// delete product using id  ---> /api/v1/admin/product/:id
exports.deleteProduct = async(req,res, next)=>{
 
    try {
        const deletedProduct = await Product.findByIdAndDelete(req.params.id);

        if(!deletedProduct){
            return next(new ErrorHandler('Product Not Found', 404));
        }

        // deleting images associated with the product
        for(let i=0;i<deletedProduct.images.length;i++){
            await cloudinary.v2.uploader.destroy(deletedProduct.images[i].public_id)
        }
    
        res.status(200).json({
            success:  true,
            message: "Product deleted successfully",
        }) 
    } catch (error) {
        if (error.name === 'CastError') {
            const message = `Resource not found. Invalid: ${error.path}`
            return res.status(500).json({
                message 
            })
        } else{
            return res.status(500).json({
                message : error.message
            })
        }
    }
    
}

