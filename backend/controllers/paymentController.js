const Stripe = require('stripe');
const stripe = Stripe(process.env.STRIPE_SECRET_KEY);

// send stripe api key => /api/v1/stripeapi
exports.sendStripeApi = (req, res)=>{
    try{
       res.status(200).json({
           stripeAPIKey: process.env.STRIPE_API_KEY
       })
    }
    catch(error){
        return res.status(500).json({
            message : error.message
        })
    }
}

// process stripe payments => /api/v1/payment/process
exports.processPayment = async(req, res, next)=>{
    try{
        
       const paymentIntent = await stripe.paymentIntents.create({
           amount: req.body.amount,
           currency: 'inr',   //'usd'
           description: 'ShopIT - An E-Commerce Website',
           payment_method_types: ['card'],
           metadata: {
            integration_check:'accept_a_payment'
           }
       })

       res.status(200).json({
           success: true,
           client_secret: paymentIntent.client_secret
       })
    }
    catch(error){
        return res.status(500).json({
            message : error.message
        })
    }
}

