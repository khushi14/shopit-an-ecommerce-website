const OrderModel = require('../models/order');
const ProductModel = require('../models/product');

//  utils
const ErrorHandler = require('../utils/errorHandler');

// create a new order   => /api/v1/order
exports.newOrder = async(req,res,next)=>{
    try {
        const {
            orderItems,
            shippingInfo,
            itemsPrice,
            taxPrice,
            shippingPrice,
            totalPrice,
            paymentInfo
    
        } = req.body;

        const order = await OrderModel.create({
            orderItems,
            shippingInfo,
            itemsPrice,
            taxPrice,
            shippingPrice,
            totalPrice,
            paymentInfo,
            paidAt : Date.now(),
            user: req.user._id
        } )

        res.status(200).json({
           success: true,
           message: 'Order created successfully',
           order
        })

    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}

// get single order   => /api/v1/order/:id
exports.getSingleOrder = async(req,res, next)=>{
    try {
        const order = await OrderModel.findById(req.params.id).populate('user', 'name email');
        if(!order){
            return next(new ErrorHandler('No Order Found With This ID', 404));
        }
        res.status(200).json({
            success: true,
            order
        })
        
    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}

// get logged-in user orders   => /api/v1/orders/me
exports.myOrder = async(req,res, next)=>{
    try {
        const orders = await OrderModel.find({user: req.user.id}).sort({createdAt : -1})

        res.status(200).json({
            success: true,
            orders
        })
        
    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}


// ADMIN 

// get all orders  => /api/v1/admin/orders
exports.allOrders = async(req,res, next)=>{
    try {
        const orders = await OrderModel.find().populate('user', 'name email').sort({createdAt : -1});

        let totalAmount = 0;
        orders.forEach(order => {
            totalAmount += order.totalPrice;  
        })
    
        res.status(200).json({
            success: true,
            totalAmount : totalAmount.toFixed(2),
            orders            
        })
        
    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}

// update /process order  => /api/v1/admin/order/:id
exports.updateOrder = async (req,res, next)=>{
    try {
        const order = await OrderModel.findById(req.params.id);

        if(order.orderStatus === 'Delivered'){
            return next(new ErrorHandler('You have already delivered this order', 400));
        }


        if(req.body.status === 'Delivered'){
            order.orderItems.forEach(async item =>{ 
                await updateStock(item.productId, item.quantity);
            })

            order.deliveredAt = Date.now();
        }

        order.orderStatus = req.body.status;
    
        await order.save();

        res.status(200).json({
            success: true,
        })

    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}

// delete order  => /api/v1/admin/order/:id
exports.deleteOrder = async(req, res, next) => {
    try {
        const deletedOrder = await OrderModel.findByIdAndDelete(req.params.id);

        if(!deletedOrder){
            return next(new ErrorHandler('No Order Found With This ID', 404));
        }

        res.status(200).json({
            success: true,
            message : "Order Deleted Successfully Through Admin!",
        })
    } catch (error) {
        return res.status(500).json({
            message : error.message
        })
    }
}


// updatedStock Function

async function updateStock(id, qty){
    const product = await ProductModel.findById(id);

    product.stock = product.stock - qty;

    await product.save();
}