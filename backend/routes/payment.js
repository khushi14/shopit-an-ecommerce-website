const express = require('express');
const router = express.Router();

const {processPayment, sendStripeApi} = require('../controllers/paymentController');

// middlewares
const {isAuthenticated} = require('../middlewares/auth');

// send stripe api key 
router.get('/stripeapi', isAuthenticated, sendStripeApi);

// process payment
router.post('/payment/process', isAuthenticated, processPayment);

module.exports = router;