const express = require('express');
const router = express.Router();

const { 
    createProduct , 
    getProducts, 
    getSpecificProduct, 
    updateProduct, 
    deleteProduct,
    getAdminProducts,
    getSpecificProductByCategory
} = require('../controllers/productController');

// middlewares
const {isAuthenticated, authorizeRoles} = require('../middlewares/auth')

// get all products (normal user)
router.get('/products',getProducts);

// get a specifc product using product id
router.get('/product/:id', getSpecificProduct)

// get a specifc product using product category
router.get('/products/category/:category', getSpecificProductByCategory)

// get all products (admin)
router.get('/admin/products', getAdminProducts);

// create new product
router.post('/admin/product', isAuthenticated , authorizeRoles('Admin'), createProduct);

// update a product using product id  
router.put('/admin/product/:id', isAuthenticated , authorizeRoles('Admin'), updateProduct)

// delete a product using product id  
router.delete('/admin/product/:id', isAuthenticated , authorizeRoles('Admin'), deleteProduct)

module.exports = router;