const express = require('express');
const app = express();
// const dotenv = require('dotenv');
const path = require('path');

const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const cloudinary = require('cloudinary');
const fileUpload = require('express-fileupload');

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(fileUpload());

// setting up config files
if(process.env.NODE_ENV !== 'PRODUCTION')
require('dotenv').config({path:  'backend/config/config.env'});

// dotenv.config({path:  'backend/config/config.env'})

const port = process.env.PORT;

// database
const db = require('./config/database');

// import routes
const productRoutes = require('./routes/product');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const orderRoutes = require('./routes/order');
const reviewRoutes = require('./routes/review');
const paymentRoutes = require('./routes/payment')

app.use('/api/v1', productRoutes);
app.use('/api/v1', authRoutes);
app.use('/api/v1', userRoutes);
app.use('/api/v1', orderRoutes);
app.use('/api/v1', reviewRoutes);
app.use('/api/v1', paymentRoutes)

// setting up cloudinary config
cloudinary.config({
    cloud_name :  process.env.CLOUDINARY_CLOUD_NAME,
    api_key : process.env.CLOUDINARY_API_KEY,
    api_secret : process.env.CLOUDINARY_SECRET_KEY
});

// handle uncaught exceptions 
process.on('uncaughtException', err=>{
    console.log(`ERROR : ${err.message}`);
    console.log('Shutting down the server due to """UNCAUGHT EXCEPTIONS"""');
    process.exit(1)

});

if (process.env.NODE_ENV === 'PRODUCTION') {
    app.use(express.static(path.join(__dirname, '../frontend/build')))

    app.get('*', (req, res) => {
        res.sendFile(path.resolve(__dirname, '../frontend/build/index.html'))
    })
}

// middlewares
const errorMiddleware = require('./middlewares/errors');
app.use(errorMiddleware);

const server = app.listen(port, ()=>{
    console.log(`Server started on PORT: ${process.env.PORT} in ${process.env.NODE_ENV} mode.`)
})

// handle unhandled promise rejections
process.on('unhandledRejection', err=>{
    console.log(`ERROR : ${err.message}`);
    console.log('Shutting down the server due to """UNHANDLED PROMISE REJECTIONS"""');
    server.close( ()=>{
        process.exit(1)
    })
})
